const express = require('express')
const mongoose = require('./server/connection')
const bodyParser = require('body-parser')

const app = express()
const port = 3000
const router = require('./server/router/routes')



app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(router)




app.listen(port,()=>{
    console.log(`starting app on ${port}`)
})