const mongoose = require('mongoose')

mongoose.Promise = global.Promise

mongoose.connect('mongodb://mongo:27017/docker-todo-app', {useNewUrlParser: true})
.then(()=>console.log('mongoose is connected now ...'))
.catch((err)=> console.log(`error connecting mongoose ${err}`))

module.exports = mongoose